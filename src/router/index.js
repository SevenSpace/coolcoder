import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home/Home'
import Project from '@/pages/project/Project'
import Contact from '@/pages/contact/Contact'
import About from '@/pages/about/About'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/project',
      name: 'Project',
      component: Project
    }, {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }, {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})
